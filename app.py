#Importing all necessary modules and packages.
from flask import Flask, request, render_template, jsonify
from flask_sqlalchemy import SQLAlchemy
from authy.api import AuthyApiClient

#AUTHY USER configuration and authentication
authy_api = AuthyApiClient('AUTH_API_KEY')
user = authy_api.users.create(
    email='YOUR_EMAIL',
    phone='YOUR_CONTACT_NUMBER', #in (xxx-xxx-xxxx)format
    country_code=+1)

if user.ok():
    authy_id = user.id
    print(user.id)

else:
	print(user.errors()) 


#FLask APP configuration.
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)


#Model for interacting with SQLAlchemy database for accessing username and password.
class user(db.Model):
    username = db.Column(db.String, unique=True, primary_key= True)
    password = db.Column(db.String)




#route1 for OTP verification
@app.route('/verify', methods=['POST'])
def vf():
    x = request.form['tkn']
    verification = authy_api.tokens.verify(authy_id, token= int(x))
    a = verification.ok()
    if a == True:
        return render_template("home.html", name = usid)
    else:
        return jsonify({'message':'Wrong credentials.'})    

#homepage, login process will start from here
@app.route('/')
def home_page():
    return render_template("login.html")


#2FA..
@app.route('/login', methods=['GET','POST'])
def login():
    global usid
    usid = request.form['userid']
    pwd = request.form['password']

    us = user.query.get(usid)

    if us.username == usid:
        if us.password == pwd:
            sms = authy_api.users.request_sms(authy_id, {'force': True})
            return render_template("token.html")

        else:
            return jsonify({'message': "wrong credentials."})

    else:
        return jsonify({'message': "wrong credentials."})        






if __name__=='__main__':
    app.run(port= 5080, debug = True)